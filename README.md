# Spring On Aws Demo

aws上运行spring应用的演示项目，实现以下功能

1. 使用Spring security实现openId connect
2. cognito与spring security集成
3. 使用spring cloud aws访问s3 parameterStore rds sqs等服务

## aws构筑手顺
### 需要预先安装
* jdk17
* nodejs
* aws cli
```
# 进入项目跟目录

# 安装cdk
npm install -g aws-cdk

# 初期化cdk
cdk bootstrap aws://{aws账户id}/{区域}

# 因为cloud formation尚不支持parameter store的加密值，所以使用aws cli创建
aws ssm put-parameter \
--name "/config/spring_cloud_aws_demo/apikey" \
--value "abcd" \
--type "SecureString"

# 手动创建存放构建成果物的存储桶
aws s3api create-bucket --bucket {存放构建成果物的桶名}

# 配置deploy_to_aws.sh文件中的如下三个环境变量
export JAVA_HOME={jdk17安装路径}
export ARTIFACT_BUCKET_NAME={存放构建成果物的桶名}
export RESOURCE_BUCKET_NAME={存储程序资源的桶名}

# 执行构建
./deploy_to_aws.sh
```

## 构建成功的输出
```
✅  SpringCloudAwsDemoStack

✨  Deployment time: 1198.55s

Outputs:
SpringCloudAwsDemoStack.SwaggerUIUrl = http://spring-cloud-aws-demo-alb-728238776.us-east-1.elb.amazonaws.com/web/swagger-ui/index.html
Stack ARN:
arn:aws:cloudformation:us-east-1:017773728365:stack/SpringCloudAwsDemoStack/3b0609e0-8ae8-11ec-ae97-0aaffc5fdae7

✨  Total time: 1222.44s
```

## 通过输出中的SwaggerUIUrl访问swagger ui
![alt text](swagger.png)
* 用户名: admin
* 密码: 123456

## 本地开发通过oidc方式认证
http://localhost:8080/web/oauth2/authorization/app-client-oidc

* 用户名: user1@test.com
* 密码: 123456

## License

The project is released under version 2.0 of the [Apache License](https://www.apache.org/licenses/LICENSE-2.0).



