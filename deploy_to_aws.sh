#!/bin/sh

export JAVA_HOME=
export ARTIFACT_BUCKET_NAME=
export RESOURCE_BUCKET_NAME=

./gradlew bootJar

aws s3 cp rds_init.sh s3://"${ARTIFACT_BUCKET_NAME}"/rds_init.sh
aws s3 cp init.sql s3://"${ARTIFACT_BUCKET_NAME}"/init.sql
aws s3 cp auth/build/libs/auth-0.0.1-SNAPSHOT.jar s3://"${ARTIFACT_BUCKET_NAME}"/auth-0.0.1-SNAPSHOT.jar
aws s3 cp web/build/libs/web-0.0.1-SNAPSHOT.jar s3://"${ARTIFACT_BUCKET_NAME}"/web-0.0.1-SNAPSHOT.jar

cd infra/cdk || exit
npm install
cdk deploy