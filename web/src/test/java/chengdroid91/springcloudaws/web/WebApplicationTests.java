/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.web;

import chengdroid91.springcloudaws.web.dto.Person;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import io.awspring.cloud.messaging.core.QueueMessagingTemplate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.WritableResource;
import org.springframework.util.StreamUtils;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 彭程
 */
@SpringBootTest
class WebApplicationTests {

    @Value("${apikey}")
    private String apiKey;

    @Value("${bucketName}")
    private String bucketName;

    @Autowired
    private ResourceLoader resourceLoader;

    private final QueueMessagingTemplate queueMessagingTemplate;

    @Autowired
    public WebApplicationTests(AmazonSQSAsync amazonSQSAsync) {
        this.queueMessagingTemplate = new QueueMessagingTemplate(amazonSQSAsync);
    }

    @Test
    void testParamStore() {
        assertEquals("abcd",apiKey);
    }

    @Test
    void testS3Resource() throws Exception {
        // 从classpath中取得要往s3上传的文件
        var testFileResource = this.resourceLoader.getResource("classpath:test.txt");
        assertTrue(testFileResource.exists());

        // s3对象资源
        var s3ObjectResource = this.resourceLoader.getResource("s3://" + bucketName + "/test.txt");

        // 将文件上传到s3
        if (s3ObjectResource instanceof WritableResource writableS3ObjectResource)  {
            try (var outputStream = writableS3ObjectResource.getOutputStream();
                 var inputStream = testFileResource.getInputStream() ) {
                StreamUtils.copy(inputStream,outputStream);
            }
        } else {
          throw new IllegalStateException("code unReachable");
        }

        // 校验上传成功
        assertTrue(s3ObjectResource.exists());

        // 下载s3文件内容并校验
        try (var inputStream = s3ObjectResource.getInputStream()) {
            var fileContent = StreamUtils.copyToString(inputStream, StandardCharsets.UTF_8);
            assertEquals("hello word",fileContent);
        }
    }

    @Test
    void testSQSMessage() {
        // 将对象转换成消息发送到sqs
        this.queueMessagingTemplate.convertAndSend("spring-cloud-aws-demo-sqs", new Person("John", 10));

        // 从sqs获取消息并转成对象
        var person = this.queueMessagingTemplate.receiveAndConvert("spring-cloud-aws-demo-sqs", Person.class);

        // 校验获取的结果
        assertNotNull(person);
        assertEquals("John",person.name());
        assertEquals(10,person.age());
    }
}
