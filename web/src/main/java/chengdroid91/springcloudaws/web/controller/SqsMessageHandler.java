/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.web.controller;

import com.amazonaws.services.s3.event.S3EventNotification;
import io.awspring.cloud.messaging.listener.SqsMessageDeletionPolicy;
import io.awspring.cloud.messaging.listener.annotation.SqsListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * @author 彭程
 */
@Component
@Slf4j
@Profile("aws")
public class SqsMessageHandler {
    @SqsListener(value = "spring-cloud-aws-demo-s3-event-sqs", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    public void queueListener(S3EventNotification s3EventNotificationRecord) {
        log.info("s3EventNotificationRecord:{}",s3EventNotificationRecord.toJson());
    }
}
