/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.web.controller;

import chengdroid91.springcloudaws.common.entity.User;
import chengdroid91.springcloudaws.common.exception.ErrorCodeEnum;
import chengdroid91.springcloudaws.common.exception.NotFoundException;
import chengdroid91.springcloudaws.common.service.UserService;
import chengdroid91.springcloudaws.common.utils.ModelMapperUtils;
import chengdroid91.springcloudaws.swagger.dto.EmptyDto;
import chengdroid91.springcloudaws.swagger.user.api.UsersApi;
import chengdroid91.springcloudaws.swagger.user.dto.CreateOrUpdateUserDto;
import chengdroid91.springcloudaws.swagger.user.dto.PageUserDto;
import chengdroid91.springcloudaws.swagger.user.dto.UserDto;
import chengdroid91.springcloudaws.web.context.ContextUser;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * @author 彭程
 */
@RestController
@RequestMapping("api")
public class UserController extends BaseController implements UsersApi {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("" +
            "hasRole('ROLE_ADMIN') " +
            "or " +
            "hasRole('ROLE_WRITE_USER') "
    )
    @Override
    public ResponseEntity<UserDto> createUser(CreateOrUpdateUserDto createOrUpdateUserDto) {
        return ResponseEntity.ok(userService.createUser(createOrUpdateUserDto));
    }

    @PreAuthorize("" +
            "hasRole('ROLE_ADMIN') " +
            "or " +
            "hasRole('ROLE_WRITE_USER') "
    )
    @Override
    public ResponseEntity<EmptyDto> deleteUserById(String userId) {
        userService.removeById(userId);
        return ResponseEntity.ok(new EmptyDto());
    }

    @Override
    @PreAuthorize("" +
            "hasRole('ROLE_ADMIN') " +
            "or " +
            "hasRole('ROLE_READ_USER') "
    )
    public ResponseEntity<UserDto> getUserById(String userId) {
        return ResponseEntity.ok(userService.getUserDetail(userId));
    }

    @Override
    @PreAuthorize("" +
            "hasRole('ROLE_ADMIN') " +
            "or " +
            "hasRole('ROLE_READ_USER') "
    )
    public ResponseEntity<PageUserDto> listUsers(Long current, Long size) {

        var contextUser = ContextUser.current();

        var page = Page.<User>of(
                Optional.ofNullable(current).orElse(1L),
                Optional.ofNullable(size).orElse(Long.MAX_VALUE)
        );
        var usersIPage = userService.page(page);

        var pageUserDto = new PageUserDto()
                .current(usersIPage.getCurrent())
                .total(usersIPage.getTotal())
                .size(usersIPage.getSize())
                .records(usersIPage.getRecords().stream()
                        .map(user -> ModelMapperUtils.map(user, UserDto.class)).toList());

        return ResponseEntity.ok(pageUserDto);
    }

    @PreAuthorize("" +
            "hasRole('ROLE_ADMIN') " +
            "or " +
            "hasRole('ROLE_WRITE_USER') "
    )
    @Override
    public ResponseEntity<UserDto> updateUserById(String userId, CreateOrUpdateUserDto createOrUpdateUserDto) {
        var user = ModelMapperUtils.map(createOrUpdateUserDto, User.class);
        user.setId(userId);
        if (!userService.updateById(user)) {
            throw new NotFoundException(ErrorCodeEnum.USER_NOT_FOUND);
        }
        var userDto = ModelMapperUtils.map(user, UserDto.class);
        return ResponseEntity.ok(userDto);
    }
}
