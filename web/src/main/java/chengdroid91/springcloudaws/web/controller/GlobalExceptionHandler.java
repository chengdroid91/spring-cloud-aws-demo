/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.web.controller;

import chengdroid91.springcloudaws.swagger.dto.ExceptionDto;
import chengdroid91.springcloudaws.common.exception.BaseException;
import chengdroid91.springcloudaws.common.exception.ErrorCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 * @author 彭程
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(Throwable.class)
    protected ResponseEntity<ExceptionDto> handleException(Throwable ex, WebRequest request) {
        if (ex instanceof BaseException baseException) {
            log.warn(baseException.getCode(),baseException);
            return ResponseEntity
                    .status(baseException.getStatus())
                    .body(new ExceptionDto(baseException.getCode()));
        } else if (ex instanceof MethodArgumentNotValidException methodArgumentNotValidException) {
            log.warn("validation failed for argument.",methodArgumentNotValidException);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(new ExceptionDto(ErrorCodeEnum.PARAMETER_ERROR.name().toLowerCase()));
        } else if (ex instanceof AccessDeniedException accessDeniedException){
            log.warn("AccessDeniedException.",accessDeniedException);
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body(new ExceptionDto(ErrorCodeEnum.ACCESS_DENIED.name().toLowerCase()));
        } else {
            log.error("Internal server error.",ex);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ExceptionDto(ErrorCodeEnum.SYSTEM_ERROR.name().toLowerCase()));
        }
    }
}
