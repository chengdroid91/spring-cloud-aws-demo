/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chengdroid91.springcloudaws.web.context;

import lombok.Getter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.Optional;

/**
 * @author 彭程
 */
public class ContextUser extends User {

    @Getter
    private final String userId;

    public ContextUser(String userId, String email, Collection<? extends GrantedAuthority> authorities) {
        super(email, "", authorities);
        this.userId = userId;
    }

    public static Optional<ContextUser> current() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext :: getAuthentication)
                .map(Authentication :: getPrincipal)
                .map(principal -> {
                    if (principal instanceof ContextUser contextUser) {
                        return contextUser;
                    }
                    //TODO
                    return null;
                });
    }
}
