/*
 * Copyright 2020-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.web.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 彭程
 */
@Configuration
@Slf4j
public class GlobalSecurityConfig {

    private static final String NOOP_PASSWORD_PREFIX = "{noop}";

    @Value("${app.formAdminLogin.name}")
    private String userName;

    @Value("${app.formAdminLogin.password}")
    private String password;

    @Value("${app.formAdminLogin.enable}")
    private boolean isFormAdminLoginEnabled;

    /**
     * Config a {@link InMemoryUserDetailsManager} to manager user login from admin login UI.
     */
    public InMemoryUserDetailsManager inMemoryUserDetailsManager() {
        return new InMemoryUserDetailsManager(
                User.withUsername(userName).password(NOOP_PASSWORD_PREFIX + password)
                        .roles("ADMIN").build());
    }

    @Bean
    @Order()
    SecurityFilterChain sessionSecurityFilterChain(HttpSecurity http) throws Exception {
        var authorizeRequests = http.authorizeRequests()
                // public the health check endpoint.
                .antMatchers("/actuator/health").permitAll();

        if (isFormAdminLoginEnabled) {
            authorizeRequests.anyRequest().hasRole("ADMIN");
        } else {
            authorizeRequests.anyRequest().denyAll();
        }

        if (isFormAdminLoginEnabled) {
            http.userDetailsService(inMemoryUserDetailsManager());
            http.formLogin()
                .successHandler((HttpServletRequest request, HttpServletResponse response,
                Authentication authentication) -> response.sendRedirect("/web/swagger-ui/index.html"));
        }

        http.csrf().ignoringAntMatchers("/api/**");
        return http.build();
    }
}
