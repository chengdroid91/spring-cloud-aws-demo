/*
 * Copyright 2020-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.SecurityFilterChain;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.springframework.security.config.Customizer.withDefaults;

/**
 * @author 彭程
 */
@Configuration
public class Oauth2LoginSecurityConfig {

	@Value("${app.oauth2LoginRedirectUrl}")
	private String oauth2LoginRedirectUrl;

	@Bean
	@Order(Ordered.HIGHEST_PRECEDENCE)
	SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http
			.requestMatchers()
			.antMatchers("/oauth2/authorization/**","/login/oauth2/**")
			.and()
			.authorizeRequests(authorizeRequests ->
				authorizeRequests.anyRequest().authenticated()
			)
			.oauth2Login(oauth2Login ->{
						oauth2Login.loginPage("/oauth2/authorization/app-client-oidc");
						oauth2Login.successHandler((HttpServletRequest request, HttpServletResponse response,
								Authentication authentication) -> {
							if (authentication instanceof OAuth2AuthenticationToken oauth2AuthenticationToken) {
								if (oauth2AuthenticationToken.getPrincipal() instanceof DefaultOidcUser oidcUser) {
									// Redirect to FrondEnd with access token.
									response.sendRedirect(oauth2LoginRedirectUrl + "/#/authSuccess/"+oidcUser.getIdToken().getTokenValue());
									return;
								}
							}
							response.sendRedirect(oauth2LoginRedirectUrl + "/#/authFailed");
						});
					}
				)
			.oauth2Client(withDefaults());
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);
		return http.build();
	}
}
