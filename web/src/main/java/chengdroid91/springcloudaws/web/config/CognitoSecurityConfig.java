/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chengdroid91.springcloudaws.web.config;

import chengdroid91.springcloudaws.common.service.UserService;
import io.awspring.cloud.autoconfigure.security.CognitoAuthenticationProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationFilter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestHeaderRequestMatcher;

import java.util.ArrayList;

/**
 * @author 彭程
 */
@Configuration
@Slf4j
@ConditionalOnProperty(prefix = "app", name = "cognitoSecurityEnabled", havingValue = "true")
@EnableConfigurationProperties(CognitoAuthenticationProperties.class)
public class CognitoSecurityConfig {
    private final CognitoAuthenticationProperties cognitoAuthenticationProperties;
    private final UserService userService;

    @Autowired
    public CognitoSecurityConfig(CognitoAuthenticationProperties cognitoAuthenticationProperties,UserService userService) {
        this.cognitoAuthenticationProperties = cognitoAuthenticationProperties;
        this.userService = userService;
    }

    /**
     * Config a {@link OAuth2TokenValidator} to check token that issued from cognito.
     */
    public DelegatingOAuth2TokenValidator<Jwt> cognitoJwtDelegatingValidator() {
        var validators = new ArrayList<OAuth2TokenValidator<Jwt>>();
        validators.add(JwtValidators.createDefaultWithIssuer(this.cognitoAuthenticationProperties.getIssuer()));
        validators.add(new JwtClaimValidator<String>("client_id",
                clientId -> clientId != null && clientId.equals(this.cognitoAuthenticationProperties.getAppClientId())));

        return new DelegatingOAuth2TokenValidator<>(validators);
    }

    /**
     * Config a {@link JwtDecoder} to decode jwt from cognito.
     */
    public JwtDecoder cognitoJwtDecoder() {
        var jwtDecoder = NimbusJwtDecoder.withJwkSetUri(this.cognitoAuthenticationProperties.getRegistry())
                .jwsAlgorithm(SignatureAlgorithm.from(this.cognitoAuthenticationProperties.getAlgorithm())).build();
        jwtDecoder.setJwtValidator(cognitoJwtDelegatingValidator());

        return jwtDecoder;
    }

    /**
     * Config a {@link SecurityFilterChain} to check cognito access token.
     */
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    SecurityFilterChain jwtSecurityFilterChain(HttpSecurity http) throws Exception {
        http.requestMatcher(new AndRequestMatcher(new AntPathRequestMatcher("/api/**"),
                new RequestHeaderRequestMatcher("AuthType","cognito")))
            .authorizeRequests((requests) -> requests.anyRequest().authenticated())
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .addFilterAfter(new CognitoContextUserFilter(this.userService), BearerTokenAuthenticationFilter.class)
            .csrf().disable()
            .oauth2ResourceServer().jwt().decoder(cognitoJwtDecoder());
        return http.build();
    }
}
