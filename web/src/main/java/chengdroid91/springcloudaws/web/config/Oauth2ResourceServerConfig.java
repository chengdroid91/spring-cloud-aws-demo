/*
 * Copyright 2020-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.web.config;

import chengdroid91.springcloudaws.common.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationFilter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestHeaderRequestMatcher;

/**
 * @author 彭程
 */
@Configuration
public class Oauth2ResourceServerConfig {

    private final UserService userService;

    @Autowired
    public Oauth2ResourceServerConfig(UserService userService) {
        this.userService = userService;
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    SecurityFilterChain resourceSecurityFilterChain(HttpSecurity http) throws Exception {
        http.requestMatcher(new AndRequestMatcher(new AntPathRequestMatcher("/api/**"),
                        new RequestHeaderRequestMatcher("AuthType","openid")))
            .authorizeRequests((requests) -> requests.anyRequest().authenticated())
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .oauth2ResourceServer()
            .jwt();

        http.addFilterAfter(new Oauth2ContextUserFilter(this.userService), BearerTokenAuthenticationFilter.class);
        return http.build();
    }
}
