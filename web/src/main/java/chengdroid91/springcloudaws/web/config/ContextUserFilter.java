/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chengdroid91.springcloudaws.web.config;

import chengdroid91.springcloudaws.common.service.UserService;
import chengdroid91.springcloudaws.swagger.user.dto.RoleDto;
import chengdroid91.springcloudaws.web.context.ContextUser;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

/**
 * @author 彭程
 */
public abstract class ContextUserFilter extends OncePerRequestFilter {

    private final UserService userService;

    public ContextUserFilter(UserService userService) {
        this.userService = userService;
    }

    protected abstract Optional<String> getEmailFromAuthentication(Authentication authentication);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        var email = getEmailFromAuthentication(authentication)
                .orElseThrow(()-> new InsufficientAuthenticationException("Can not determine email from authentication"));

        var user = userService.findUserByEmail(email);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("user not found");
        }

        var userDto = userService.getUserDetail(user.get().getId());
        var authorities =  userDto
                .getRoles()
                .stream()
                .map(RoleDto::getRoleName)
                .map(SimpleGrantedAuthority::new)
                .toList();

        var contextUser = new ContextUser(userDto.getId(),userDto.getEmail(), authorities);

        var context = SecurityContextHolder.createEmptyContext();
        context.setAuthentication(new Authentication() {

            @Override
            public String getName() {
                return userDto.getId();
            }

            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return authorities;
            }

            @Override
            public Object getCredentials() {
                return "";
            }

            @Override
            public Object getDetails() {
                return contextUser;
            }

            @Override
            public Object getPrincipal() {
                return contextUser;
            }

            @Override
            public boolean isAuthenticated() {
                return true;
            }

            @Override
            public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

            }
        });
        SecurityContextHolder.setContext(context);

        filterChain.doFilter(request,response);
    }
}
