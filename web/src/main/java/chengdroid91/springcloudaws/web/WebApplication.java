/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.web;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import io.awspring.cloud.core.region.RegionProvider;
import io.awspring.cloud.messaging.config.QueueMessageHandlerFactory;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.PayloadMethodArgumentResolver;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import java.util.Collections;

/**
 * @author 彭程
 */
@MapperScan("chengdroid91.springcloudaws.common.mapper")
@SpringBootApplication(scanBasePackages = "chengdroid91.springcloudaws")
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true,jsr250Enabled = true)
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }

    @Bean
    public OpenAPI springCloudAwsDemoAPI() {
        return new OpenAPI()
                .info(new Info().title("Spring Cloud Aws Demo API")
                        .description("demo application of spring cloud aws")
                        .contact(new Contact().name("chengdroid91"))
                        .version("v0.0.1"));
    }

    /**
     * https://docs.awspring.io/spring-cloud-aws/docs/current/reference/html/index.html#consuming-aws-event-messages-with-amazon-sqs
     */
    @Bean
    public QueueMessageHandlerFactory queueMessageHandlerFactory() {
        QueueMessageHandlerFactory factory = new QueueMessageHandlerFactory();
        MappingJackson2MessageConverter messageConverter = new MappingJackson2MessageConverter();

        //set strict content type match to false
        messageConverter.setStrictContentTypeMatch(false);
        factory.setArgumentResolvers(Collections.singletonList(new PayloadMethodArgumentResolver(messageConverter)));
        return factory;
    }

    /**
     * https://docs.awspring.io/spring-cloud-aws/docs/current/reference/html/index.html#fifo-queue-support
     */
    @Bean
    AmazonSQSAsync amazonSQS(AWSCredentialsProvider awsCredentialsProvider, RegionProvider regionProvider) {
        return AmazonSQSAsyncClientBuilder.standard().withCredentials(awsCredentialsProvider)
                .withRegion(regionProvider.getRegion().getName()).build();
    }

}
