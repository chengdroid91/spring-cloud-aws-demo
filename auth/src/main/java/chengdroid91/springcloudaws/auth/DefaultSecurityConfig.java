/*
 * Copyright 2020-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.auth;

import chengdroid91.springcloudaws.common.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

/**
 * @author 彭程
 */
@EnableWebSecurity
public class DefaultSecurityConfig {

    private final UserService userService;

    @Autowired
    public DefaultSecurityConfig(UserService userService) {
        this.userService = userService;
    }

    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(authorizeRequests -> {
                            authorizeRequests.antMatchers("/actuator/health").permitAll();
                            authorizeRequests.anyRequest().denyAll();
                        }
                )
                .formLogin(withDefaults());
        return http.build();
    }

    /**
     * Register the {@link UserDetailsService}.
     */
    @Bean
    UserDetailsService users() {

        return username -> {

            var lambdaQueryWrapper = new LambdaQueryWrapper<chengdroid91.springcloudaws.common.entity.User>();
            lambdaQueryWrapper.eq(chengdroid91.springcloudaws.common.entity.User::getEmail, username);

            var user = userService.getOne(lambdaQueryWrapper);

            if (user == null) {
                throw new UsernameNotFoundException(username + " is not found");
            }

            return User
                    .withUsername(user.getEmail())
                    .password(user.getPassword())
                    .roles("user")
                    .build();
        };
    }
}
