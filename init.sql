CREATE DATABASE `spring-cloud-aws-demo` DEFAULT CHARACTER SET utf8mb4;

use `spring-cloud-aws-demo`;

CREATE TABLE `user`
(
    id             bigint auto_increment  primary key,
    name           varchar(30)  null,
    age            int          null,
    email          varchar(50)  null,
    password       varchar(100) not null,
    create_time    timestamp    null,
    update_time    timestamp    null,
    create_user_id bigint       null,
    update_user_id bigint       null,
    constraint user_email_uindex unique (email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO user (name, age, email, password, create_time, update_time, create_user_id, update_user_id)
VALUES ('admin', 30, 'user1@test.com', '{bcrypt}$2a$10$dXJ3SW6G7P50lGmMkkmwe.qYxvoegUzTMvpbXHIrcgcWbnlT4M1Tu',now(), now(), 1, 1);

create table role_master
(
    id        int auto_increment,
    role_name varchar(20) not null,
    create_time    timestamp    null,
    update_time    timestamp    null,
    create_user_id bigint       null,
    update_user_id bigint       null,
    constraint role_master_pk
        primary key (id)
);

INSERT INTO role_master (role_name, create_time, update_time, create_user_id, update_user_id)
VALUES ('ROLE_ADMIN',now(), now(), 1, 1);

INSERT INTO role_master (role_name, create_time, update_time, create_user_id, update_user_id)
VALUES ('ROLE_READ_USER',now(), now(), 1, 1);

INSERT INTO role_master (role_name, create_time, update_time, create_user_id, update_user_id)
VALUES ('ROLE_WRITE_USER',now(), now(), 1, 1);

create table user_role
(
    user_id        bigint not null,
    role_master_id int not null,
    create_time    timestamp    null,
    update_time    timestamp    null,
    create_user_id bigint       null,
    update_user_id bigint       null,
    constraint user_role_pk
        primary key (user_id,role_master_id),
    constraint user_role_role_master__fk
        foreign key (role_master_id) references role_master (id)
            on update cascade on delete cascade,
    constraint user_role_user__fk
        foreign key (user_id) references user (id)
            on update cascade on delete cascade
);

INSERT INTO user_role (user_id, role_master_id, create_time, update_time, create_user_id, update_user_id) VALUES (1, 1,now(), now(), 1, 1);



