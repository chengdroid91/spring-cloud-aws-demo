/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.common.mapper;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import io.awspring.cloud.jdbc.config.annotation.RdsInstanceConfigurer;
import io.awspring.cloud.jdbc.datasource.DataSourceFactory;
import io.awspring.cloud.jdbc.datasource.DataSourceInformation;
import org.apache.tomcat.jdbc.pool.PoolConfiguration;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

/**
 * @author 彭程
 */
@Configuration
public class MyBatisPlusConfig {

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

    @Bean
    @Profile("aws")
    public RdsInstanceConfigurer instanceConfigurer() {
        return () -> new DataSourceFactory() {
            @Override
            public DataSource createDataSource(DataSourceInformation dataSourceInformation) {
                // create a method scoped instance
                PoolConfiguration configurationToUse = new PoolProperties();

                configurationToUse.setDriverClassName("com.amazonaws.secretsmanager.sql.AWSSecretsManagerMySQLDriver");
                configurationToUse.setUrl(String.format("jdbc-secretsmanager:mysql://%s:%s/%s",
                        dataSourceInformation.getHostName(),
                        dataSourceInformation.getPort(),
                        dataSourceInformation.getDatabaseName()));
                configurationToUse.setUsername(dataSourceInformation.getUserName());
                return new org.apache.tomcat.jdbc.pool.DataSource(configurationToUse);
            }

            @Override
            public void closeDataSource(DataSource dataSource) {
                if (dataSource instanceof org.apache.tomcat.jdbc.pool.DataSource tomcatDataSource) {
                    tomcatDataSource.close();
                }
            }
        };
    }
}
