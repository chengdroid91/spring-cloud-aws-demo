/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.common.service.implement;

import chengdroid91.springcloudaws.common.entity.User;
import chengdroid91.springcloudaws.common.entity.UserRole;
import chengdroid91.springcloudaws.common.exception.BusinessException;
import chengdroid91.springcloudaws.common.exception.ErrorCodeEnum;
import chengdroid91.springcloudaws.common.exception.NotFoundException;
import chengdroid91.springcloudaws.common.mapper.UserMapper;
import chengdroid91.springcloudaws.common.mapper.UserRoleMapper;
import chengdroid91.springcloudaws.common.service.UserService;
import chengdroid91.springcloudaws.common.utils.ModelMapperUtils;
import chengdroid91.springcloudaws.swagger.user.dto.CreateOrUpdateUserDto;
import chengdroid91.springcloudaws.swagger.user.dto.RoleDto;
import chengdroid91.springcloudaws.swagger.user.dto.UserDto;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Optional;

/**
 * @author 彭程
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<UserMapper,User> implements UserService {

    private final UserRoleMapper userRoleMapper;

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    public UserServiceImpl(UserRoleMapper userRoleMapper) {
        this.userRoleMapper = userRoleMapper;
    }

    @Override
    public Optional<User> findUserByEmail(String email) {
        var lambdaQueryWrapper = new LambdaQueryWrapper<User>();
        lambdaQueryWrapper.eq(User::getEmail, email);
        return Optional.ofNullable(this.baseMapper.selectOne(lambdaQueryWrapper));
    }

    @Override
    @Transactional
    public UserDto createUser(CreateOrUpdateUserDto createOrUpdateUserDto) {

        if (findUserByEmail(createOrUpdateUserDto.getEmail()).isPresent()) {
            throw new BusinessException(ErrorCodeEnum.USER_ALREADY_EXISTED);
        }

        var user = ModelMapperUtils.map(createOrUpdateUserDto, User.class);
        user.setPassword("{bcrypt}" + passwordEncoder.encode("123456"));

        this.baseMapper.insert(user);

        if (!CollectionUtils.isEmpty(createOrUpdateUserDto.getRoles())) {
            for (RoleDto roleDto : createOrUpdateUserDto.getRoles()) {
                var userRole = new UserRole();
                userRole.setUserId(user.getId());
                userRole.setRoleMasterId(roleDto.getId());
                this.userRoleMapper.insert(userRole);
            }
        }

        var userDto = ModelMapperUtils.map(user, UserDto.class);
        userDto.setRoles(createOrUpdateUserDto.getRoles());

        return this.getUserDetail(user.getId());
    }

    @Override
    public UserDto getUserDetail(String userId) {
        var user = this.baseMapper.findOneById(userId);
        if (user == null) {
            throw new NotFoundException(ErrorCodeEnum.USER_NOT_FOUND);
        }

        var userDto = ModelMapperUtils.map(user, UserDto.class);
        userDto.setRoles(ModelMapperUtils.mapList(user.getRoleMasterList(), RoleDto.class));
        return userDto;
    }
}
