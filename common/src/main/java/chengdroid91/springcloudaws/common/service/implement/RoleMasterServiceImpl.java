/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chengdroid91.springcloudaws.common.service.implement;

import chengdroid91.springcloudaws.common.entity.RoleMaster;
import chengdroid91.springcloudaws.common.mapper.RoleMasterMapper;
import chengdroid91.springcloudaws.common.service.RoleMasterService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 彭程
 */
@Service
@Transactional(readOnly = true)
public class RoleMasterServiceImpl extends BaseServiceImpl<RoleMasterMapper, RoleMaster> implements RoleMasterService {
}
