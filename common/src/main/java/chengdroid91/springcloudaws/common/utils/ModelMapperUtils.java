/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.common.utils;

import org.modelmapper.ModelMapper;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

/**
 * @author 彭程
 */
public class ModelMapperUtils {

    private final static ModelMapper modelMapper = new ModelMapper();

    public static <T> T map(Object source, Class<T> destinationType) {
        return modelMapper.map(source,destinationType);
    }

    public static <T,S> List<T> mapList(List<S> sourceList, Class<T> destinationType) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return Collections.emptyList();
        }
        return  sourceList.stream().map(source -> map(source, destinationType)).toList();
    }
}
