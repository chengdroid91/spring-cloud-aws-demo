/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chengdroid91.springcloudaws.common.exception;

/**
 * @author 彭程
 */
public abstract class BaseException extends RuntimeException{

    private final ErrorCodeEnum errorCode;

    public BaseException(ErrorCodeEnum errorCodeEnum) {
        this.errorCode = errorCodeEnum;
    }

    public String getCode() {
        return errorCode.name().toLowerCase();
    }

    public abstract int getStatus();
}
