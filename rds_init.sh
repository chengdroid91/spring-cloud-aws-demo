#!/bin/bash

set -e

aws configure set region "$AWS_REGION"

secretId="/secret/spring_cloud_aws_demo/rds"

db_secret_string=$(aws secretsmanager get-secret-value --secret-id "${secretId}" | jq --raw-output '.SecretString')
db_user=$(echo "${db_secret_string}" | jq -r .username)
db_password=$(echo "${db_secret_string}" | jq -r .password)
db_url=$(echo "${db_secret_string}" | jq -r .host)

export DB_USER=${db_user}
export DB_PASSWORD=${db_password}
export DB_URL=${db_url}

MYSQL="mysql -h${DB_URL} -u${DB_USER} -p${DB_PASSWORD} --default-character-set=utf8mb4 -A -N"

$MYSQL <<EOFMYSQL
source init.sql;
EOFMYSQL