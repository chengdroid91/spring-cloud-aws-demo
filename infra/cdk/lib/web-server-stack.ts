/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Construct} from "constructs";
import {
    Duration,
    NestedStack,
    NestedStackProps,
    Tags
} from 'aws-cdk-lib';

import {aws_ec2 as ec2} from 'aws-cdk-lib';
import {aws_s3 as s3} from 'aws-cdk-lib';
import {aws_autoscaling as autoscaling} from 'aws-cdk-lib';
import {InitPackage, SubnetType, InitFile, InitService, InitUser, InitCommand} from "aws-cdk-lib/aws-ec2";
import {HealthCheck, Signals} from "aws-cdk-lib/aws-autoscaling";
import {aws_elasticloadbalancingv2 as elb} from 'aws-cdk-lib';
import {aws_iam as iam} from 'aws-cdk-lib';

interface AlbStackProps extends NestedStackProps {
    vpc: ec2.Vpc
    albListener: elb.ApplicationListener
    ec2Sg: ec2.SecurityGroup
    ec2Role: iam.Role
    rdsInstanceIdentifier: string
    resourceBucket: s3.Bucket
    artifactBucket: s3.IBucket
    host: string
    userPoolId: string
    userPoolClientId: string
}

/**
 * @author 彭程
 */
class WebServerStack extends NestedStack {

    constructor(scope: Construct, id: string, props: AlbStackProps) {
        super(scope, id, props);

        // Use Latest Amazon Linux Image - CPU Type ARM64
        const ami = new ec2.AmazonLinuxImage({
            generation: ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
            cpuType: ec2.AmazonLinuxCpuType.ARM_64
        });

        // the command for user data
        const commandsUserData = ec2.UserData.forLinux();
        commandsUserData.addCommands(
            "yum update -y");

        // the cloudFormationInit
        const cloudFormationInit = ec2.CloudFormationInit.fromConfigSets({
            configSets: {
                default: ["setupCloudWatchAgent","setupJavaPackage", "startApp"],
                withInit: ["setupCloudWatchAgent","setupJavaPackage", "startApp"]
            },
            configs: {
                setupCloudWatchAgent: new ec2.InitConfig([
                    ec2.InitPackage.yum('amazon-cloudwatch-agent'),
                    ec2.InitFile.fromObject('/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json', {
                            "metrics": {
                                "namespace": "spring_cloud_aws_demo_ec2_metrics",
                                "append_dimensions": {
                                    "AutoScalingGroupName": "${aws:AutoScalingGroupName}",
                                    "ImageId": "${aws:ImageId}",
                                    "InstanceId": "${aws:InstanceId}",
                                    "InstanceType": "${aws:InstanceType}"
                                },
                                "metrics_collected": {
                                    "mem": {
                                        "measurement": [
                                            "mem_used_percent"
                                        ]
                                    },
                                    "disk": {
                                        "measurement": [
                                            "used_percent"
                                        ]
                                    }
                                }
                            },
                            "logs": {
                                "logs_collected": {
                                    "files": {
                                        "collect_list": [
                                            {
                                                "file_path": "/var/log/cfn-init.log",
                                                "log_group_name": "/spring-boot-aws-demo/web/instances/cfninit",
                                                "log_stream_name": "{instance_id}",
                                                "timezone": "UTC"
                                            },
                                            {
                                                "file_path": "/home/ec2-user/logs/app.log",
                                                "log_group_name": "/spring-boot-aws-demo/web/instances/app-log",
                                                "log_stream_name": "{instance_id}",
                                                "timezone": "UTC"
                                            }
                                        ]
                                    }
                                },
                            }
                        },
                    ),
                    InitCommand.shellCommand("sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:/opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json")
                ]),
                setupJavaPackage: new ec2.InitConfig([
                    InitPackage.yum("java-17-amazon-corretto-headless"),
                ]),
                startApp: new ec2.InitConfig([
                    InitFile.fromString("/etc/systemd/system/app.service",
                        [
                            "[Unit]",
                            "Description=spring-cloud-aws-demo-web",
                            "After=syslog.target",
                            "",
                            "[Service]",
                            "User=ec2-user",
                            "ExecStart=/home/ec2-user/web.jar",
                            "SuccessExitStatus=143",
                            "",
                            "[Install]",
                            "WantedBy=multi-user.target"
                        ].join("\n"),
                        {
                            mode: "000550",
                            owner: "ec2-user"
                        }),
                    InitFile.fromS3Object("/home/ec2-user/web.jar", props.artifactBucket, "web-0.0.1-SNAPSHOT.jar", {
                        mode: "000550",
                        owner: "ec2-user"
                    }),
                    InitFile.fromString("/home/ec2-user/web.conf",
                        [
                            "LOG_FOLDER=/dev",
                            "LOG_FILENAME=null",
                            "RUN_AS_USER=ec2-user",
                            `JAVA_OPTS="${[
                                `-Xmx1500M`,
                                `-Dspring.profiles.active=aws`,
                                `-DAWS_REGION=${this.region}`,
                                `-DRDS_INSTANCE_IDENTIFIER=${props.rdsInstanceIdentifier}`,
                                `-DBUCKET_NAME=${props.resourceBucket.bucketName}`,
                                `-Dspring.security.oauth2.resourceserver.jwt.issuer-uri=http://${props.host}/auth`,
                                `-Dspring.security.oauth2.client.provider.spring.issuer-uri=http://${props.host}/auth`,
                                `-Dspring.security.oauth2.client.registration.app-client-oidc.redirect-uri=http://${props.host}/web/login/oauth2/code/app-client-oidc`,
                                `-Dspring.cloud.aws.security.cognito.userPoolId=${props.userPoolId}`,
                                `-Dspring.cloud.aws.security.cognito.appClientId=${props.userPoolClientId}`,
                            ].join(" ")}"`,
                        ].join("\n"),
                        {
                            mode: "000550",
                            owner: "ec2-user"
                        }),
                    InitService.enable("app", {
                        enabled: true,
                        ensureRunning: true
                    })
                ])
            }
        })

        // Auto scaling group
        const autoScalingGroup = new autoscaling.AutoScalingGroup(this, 'asg-spring-cloud-aws-demo-web', {
            vpc: props.vpc,
            autoScalingGroupName: "spring-cloud-aws-demo-web-asg",
            instanceType: ec2.InstanceType.of(ec2.InstanceClass.T4G, ec2.InstanceSize.SMALL),
            machineImage: ami,
            securityGroup: props.ec2Sg,
            userData: commandsUserData,
            vpcSubnets: {subnetType: SubnetType.PUBLIC},
            healthCheck: HealthCheck.elb({grace: Duration.minutes(16)}),
            role: props.ec2Role,
            init: cloudFormationInit,
            signals: Signals.waitForMinCapacity({timeout: Duration.minutes(15)}),
            initOptions: {
                configSets: ["withInit"],
                printLog: true
            }
        });
        Tags.of(autoScalingGroup).add("Name", "spring-cloud-aws-demo-web-instance");

        props.albListener.addTargets('atg-cloud-aws-demo-web', {
            protocol: elb.ApplicationProtocol.HTTP,
            port: 8080,
            priority: 2,
            conditions: [
                elb.ListenerCondition.pathPatterns(['/web/*']),
            ],
            targetGroupName: "cloud-aws-demo-web-instance-atg",
            // Enable health check by spring boot actuator health check endpoint
            healthCheck: {
                enabled: true,
                interval: Duration.seconds(5),
                path: "/web/actuator/health",
                port: "8080",
                protocol: elb.Protocol.HTTP,
                timeout: Duration.seconds(3),
                healthyThresholdCount: 2,
                unhealthyThresholdCount: 5,
                healthyHttpCodes: "200"
            },
            targets: [autoScalingGroup]
        });
    }
}

export {WebServerStack};