/*
 * Copyright 2002-2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
    aws_cognito as cognito,
    aws_ec2 as ec2,
    aws_elasticloadbalancingv2 as elb,
    aws_iam as iam,
    aws_rds as rds,
    aws_s3 as s3,
    aws_s3_notifications as s3n,
    aws_sqs as sqs,
    CfnOutput,
    CfnParameter,
    Fn,
    RemovalPolicy,
    Stack,
    StackProps
} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {SubnetType} from "aws-cdk-lib/aws-ec2";
import {Credentials, DatabaseSecret} from "aws-cdk-lib/aws-rds";
import {WebServerStack} from "./web-server-stack";
import {AuthServerStack} from "./auth-server-stack";

/**
 * @author 彭程
 */
export class CdkStack extends Stack {
    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props);

        // Create new VPC with 2 Subnets
        const vpc = new ec2.Vpc(this, 'vpc-spring-cloud-aws-demo', {
            natGateways: 0,
            vpcName: "spring-cloud-aws-demo-vpc",
            gatewayEndpoints:{
                s3Endpoint:{
                    service: ec2.GatewayVpcEndpointAwsService.S3
                }
            }
        });

        // Define the role for ec2
        const ec2Role = new iam.Role(this, 'role-spring-cloud-aws-demo', {
            roleName: "spring-cloud-aws-demo-ec2-role",
            assumedBy: new iam.ServicePrincipal('ec2.amazonaws.com')
        })

        // the security group of ec2
        const ec2SecurityGroup = new ec2.SecurityGroup(this, "sg-ec2-spring-cloud-aws-demo", {
            vpc: vpc,
            allowAllOutbound: true,
            securityGroupName: "spring-cloud-aws-demo-ec2-sg"
        });

        // manager rds secret by SecretsManager
        const rdsCredentials = new DatabaseSecret(this, 'credentials-spring-cloud-aws-demo-rds', {
            secretName: "/secret/spring_cloud_aws_demo/rds",
            username: 'admin'
        });

        // Create rds instance
        const rdsInstance = new rds.DatabaseInstance(this, 'rds-spring-cloud-aws-demo', {
            engine: rds.DatabaseInstanceEngine.mysql({version: rds.MysqlEngineVersion.VER_8_0_26}),
            instanceIdentifier: "spring-cloud-aws-demo-db",
            allocatedStorage: 20,
            instanceType: ec2.InstanceType.of(ec2.InstanceClass.T4G, ec2.InstanceSize.MICRO),
            credentials: Credentials.fromSecret(rdsCredentials),
            deletionProtection: false,
            removalPolicy: RemovalPolicy.DESTROY,
            vpc,
            vpcSubnets: {
                subnetType: SubnetType.PRIVATE_ISOLATED
            },
        });
        // Allow board instance access rds
        rdsInstance.connections.allowFrom(ec2SecurityGroup, ec2.Port.tcp(3306), 'allow access rds from board instance');

        // create queue to handle message
        const queue = new sqs.Queue(this, 'sqs-spring-cloud-aws-demo', {
            queueName: "spring-cloud-aws-demo-sqs"
        });

        // create queue to handle s3 event
        const s3EventQueue = new sqs.Queue(this, 'sqs-spring-cloud-aws-demo-s3', {
            queueName: "spring-cloud-aws-demo-s3-event-sqs"
        });

        // queue policy
        s3EventQueue.addToResourcePolicy(new iam.PolicyStatement({
            sid: "allow-s3-event",
            effect: iam.Effect.ALLOW,
            principals: [new iam.ServicePrincipal('s3.amazonaws.com')],
            resources: [s3EventQueue.queueArn],
            actions: ['SQS:SendMessage']
        }));

        // Create Bucket
        const resourceBucketName = process.env.RESOURCE_BUCKET_NAME || "spring-cloud-aws-demo";
        const resourceBucket = new s3.Bucket(this, 's3-spring-cloud-aws-demo', {
            bucketName: resourceBucketName,
            removalPolicy: RemovalPolicy.DESTROY
        });

        // Bucket to storage jar of spring boot
        const artifactBucketName = process.env.ARTIFACT_BUCKET_NAME || "spring-cloud-aws-demo-artifact";
        const artifactBucket = s3.Bucket.fromBucketName(this,'s3-artifact-spring-cloud-aws-demo', artifactBucketName);

        // Add object create notification to sqs
        resourceBucket.addEventNotification(s3.EventType.OBJECT_CREATED, new s3n.SqsDestination(s3EventQueue));

        const userPool = new cognito.UserPool(this, 'id-user-pool', {
                selfSignUpEnabled: true,
                userPoolName: "scad-user-pool",
                autoVerify: {
                    email: true,
                }
            }
        );

        const client = userPool.addClient('id-scad-app-client', {
            generateSecret:false,
            authFlows: {
                userPassword: true
            },
        });

        // Define the policy
        const policy = new iam.Policy(this, 'policy-spring-cloud-aws-demo', {
            policyName: "spring-cloud-aws-demo-policy",
            statements: [new iam.PolicyStatement({
                effect: iam.Effect.ALLOW,
                actions: [
                    "sqs:DeleteMessage",
                    "s3:PutObject",
                    "s3:GetObject",
                    "sqs:GetQueueUrl",
                    "sqs:ReceiveMessage",
                    "sqs:SendMessage",
                    "sqs:GetQueueAttributes"
                ],
                resources: [
                    `arn:aws:s3:::${resourceBucketName}/*`,
                    `arn:aws:s3:::${artifactBucketName}/*`,
                    queue.queueArn
                ],
            }),
                new iam.PolicyStatement({
                    effect: iam.Effect.ALLOW,
                    actions: [
                        "ssm:GetParametersByPath",
                        "secretsmanager:GetSecretValue",
                        "secretsmanager:DescribeSecret",
                        "sqs:DeleteMessage",
                        "sqs:GetQueueUrl",
                        "sqs:ReceiveMessage",
                        "sqs:GetQueueAttributes",
                        "rds:DescribeDBInstances"
                    ],
                    resources: [
                        `arn:aws:ssm:${this.region}:${this.account}:parameter/config*`,
                        `arn:aws:secretsmanager:${this.region}:${this.account}:secret:/secret/spring_cloud_aws_demo*`,
                        rdsInstance.instanceArn,
                        s3EventQueue.queueArn
                    ],
                })
            ],
        });

        // Attach policy to role
        policy.attachToRole(ec2Role);
        ec2Role.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName("CloudWatchAgentServerPolicy"));

        // Application load balance
        const alb = new elb.ApplicationLoadBalancer(this, 'alb-spring-cloud-aws-demo', {
            vpc,
            vpcSubnets: {subnetType: SubnetType.PUBLIC},
            loadBalancerName: "spring-cloud-aws-demo-alb",
            internetFacing: true,
            deletionProtection: false,
        });

        const albListener = alb.addListener('listener-cloud-aws-demo', {
            port: 80,
            defaultAction:elb.ListenerAction.fixedResponse(503,{
                contentType:"text/plain",
                messageBody:"503"
            })
        });

        // allow 8080 and 9000 from alb
        ec2SecurityGroup.connections.allowFrom(alb, ec2.Port.tcp(8080), 'allow 8080 from alb');
        ec2SecurityGroup.connections.allowFrom(alb, ec2.Port.tcp(9000), 'allow 9000 from alb');

        const authServerStack = new AuthServerStack(this, "stack-spring-cloud-aws-demo-auth-server", {
            albListener: albListener,
            ec2Role: ec2Role,
            ec2Sg: ec2SecurityGroup,
            vpc: vpc,
            rdsInstanceIdentifier: rdsInstance.instanceIdentifier,
            resourceBucket: resourceBucket,
            artifactBucket: artifactBucket,
            host: alb.loadBalancerDnsName
        })

        const webServerStack = new WebServerStack(this, "stack-spring-cloud-aws-demo-web-server", {
            albListener: albListener,
            ec2Role: ec2Role,
            ec2Sg: ec2SecurityGroup,
            vpc: vpc,
            rdsInstanceIdentifier: rdsInstance.instanceIdentifier,
            resourceBucket: resourceBucket,
            artifactBucket: artifactBucket,
            userPoolId: userPool.userPoolId,
            userPoolClientId: userPool.userPoolId,
            host: alb.loadBalancerDnsName
        })

        webServerStack.addDependency(authServerStack);

        new CfnOutput(this, 'Swagger UI Url', {value: `http://${alb.loadBalancerDnsName}/web/swagger-ui/index.html`});

        new CfnOutput(this, 'userPoolId', {value: userPool.userPoolId});
        new CfnOutput(this, 'userPoolClientId', {value: client.userPoolClientId});
    }
}
